import './style.css'
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import WebGL from 'three/addons/capabilities/WebGL.js';
if ( !WebGL.isWebGL2Available() ){
	console.log("WEBGL NOT AVAILABLE")
	const warning = WebGL.getWebGLErrorMessage();
	document.getElementById( 'container' ).appendChild( warning );
	//process.abort()
	//window.stop() 
	//throw new Error("NO WEBGL")
}
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const geometry = new THREE.BoxGeometry( 4, 1, 4 );
const material = new THREE.MeshStandardMaterial( { color: 0x00ff00 } );
const cube = new THREE.Mesh( geometry, material );

const light = new THREE.PointLight( 0xffffff );
light.position.set ( 0 ,2, 0 );

const ambientLight = new THREE.AmbientLight( 0xffffff );


const lightHelper = new THREE.PointLightHelper(light)
const gridHelper = new THREE.GridHelper(200,50);
scene.add(lightHelper,gridHelper)

const shape = new THREE.Mesh()
scene.add( light, ambientLight);

//light.shadow.bias = - 0.01;
scene.add( cube );

const controls = new OrbitControls(camera, renderer.domElement);
camera.position.z = 6;

function init(){
	window.addEventListener( 'resize', onWindowResize );
	// add all the camera, scene, shadows and whatever in here to clean up code.
}

function onWindowResize(){
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerHeight)
}


function animate() {
	requestAnimationFrame( animate );

	//cube.rotation.x += 0.001;
	//cube.rotation.y += 0.001;

	controls.update();

	renderer.render( scene, camera );
}

init();
animate();
